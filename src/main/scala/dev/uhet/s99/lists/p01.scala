package dev.uhet.s99.lists

import scala.annotation.tailrec

object p01 {

  @tailrec
  def last[A](list: List[A]): Option[A] = list match {
    case Nil => None
    case x :: Nil => Some(x)
    case _ :: xs => last(xs)
  }
}
