package dev.uhet.s99.lists

import scala.annotation.tailrec

object p03 {

  def nth[A](n: Int, list: List[A]): Option[A] = {

    @tailrec
    def inner(n: Int, list: List[A]): Option[A] =
      if (n == 0) list match {
        case Nil => None
        case x :: _ => Some(x)
      } else list match {
        case Nil => None
        case _ :: xs => inner(n - 1, xs)
      }

    if (n < 0) None else inner(n, list)
  }
}
