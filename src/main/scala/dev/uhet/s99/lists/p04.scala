package dev.uhet.s99.lists

import scala.annotation.tailrec

object p04 {

  def length[A](list: List[A]): Int = {

    @tailrec
    def inner(list: List[A], acc: Int): Int = list match {
      case Nil => acc
      case _ :: xs => inner(xs, acc + 1)
    }

    inner(list, 0)
  }
}
