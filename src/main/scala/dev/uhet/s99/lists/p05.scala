package dev.uhet.s99.lists

import scala.annotation.tailrec

object p05 {

  def reverse[A](list: List[A]): List[A] = {

    @tailrec
    def inner(list: List[A], acc: List[A]): List[A] = list match {
      case Nil => acc
      case x :: xs => inner(xs, x :: acc)
    }

    inner(list, Nil)
  }
}
