package dev.uhet.s99.lists

object p06 {

  def isPalindrome[A](list: List[A]): Boolean = list == p05.reverse(list)
}
