package dev.uhet.s99.lists

import scala.annotation.tailrec

object p07 {

  def concat[A](a: List[A], b: List[A]): List[A] = {

    @tailrec
    def inner(list: List[A], acc: List[A]): List[A] = list match {
      case Nil => acc
      case x :: xs => inner(xs, x :: acc)
    }

    inner(p05.reverse(a), b)
  }

  def flatten[A](list: List[List[A]]): List[A] = {

    @tailrec
    def inner(list: List[List[A]], acc: List[A]): List[A] = {
      list match {
        case Nil => acc
        case x :: xs => inner(xs, concat(acc, x))
      }
    }
    inner(list, Nil)
  }
}
