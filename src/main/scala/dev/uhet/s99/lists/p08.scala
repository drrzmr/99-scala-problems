package dev.uhet.s99.lists

import scala.annotation.tailrec

object p08 {

  def compress[A](list: List[A]): List[A] = {

    @tailrec
    def inner(list: List[A], acc: List[A]): List[A] = list match {
      case Nil => p05.reverse(acc)
      case x :: xs if x == acc.head => inner(xs, acc)
      case x :: xs => inner(xs, x :: acc)
    }

    list match {
      case Nil => Nil
      case x :: xs => inner(xs, List(x))
    }
  }
}
