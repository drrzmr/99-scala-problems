package dev.uhet.s99.lists

import scala.annotation.tailrec

object p09 {

  def takeWhileEqual[A](list: List[A]): (List[A], List[A]) = {

    @tailrec
    def inner(list: List[A], acc: List[A]): (List[A], List[A]) = {
      list match {
        case Nil => (acc, Nil)
        case x :: y :: ss if x != y => (x :: acc, y :: ss)
        case x :: xs => inner(xs, x :: acc)
      }

    }
    inner(list, Nil)
  }

  def takeWhile[A](list: List[A])(p: (A, A) => Boolean): (List[A], List[A]) = {

    @tailrec
    def inner(list: List[A], acc: List[A]): (List[A], List[A]) = {
      list match {
        case Nil => (acc, Nil)
        case x :: y :: ss if p(x, y) => (x :: acc, y :: ss)
        case x :: xs => inner(xs, x :: acc)
      }

    }
    inner(list, Nil)
  }

  def pack[A](list: List[A]): List[List[A]] = {

    @tailrec
    def inner(list: List[A], acc: List[List[A]]): List[List[A]] = {
      list match {
        case Nil => p05.reverse(acc)
        case x =>
          val (newPack, newList) = takeWhile(x)(_ != _)
          inner(newList, newPack :: acc)
      }
    }

    inner(list, Nil)
  }
}
