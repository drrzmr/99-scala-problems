package dev.uhet.s99.lists

import scala.annotation.tailrec

object p10 {

  def encode[A](list: List[A]): List[(Int, A)] = {

    @tailrec
    def inner(packed: List[List[A]], acc: List[(Int, A)]): List[(Int, A)] = packed match {
      case Nil => p05.reverse(acc)
      case x :: xs => inner(xs, (x.length, x.head) :: acc)
    }

    inner(p09.pack[A](list), List.empty[(Int, A)])

  }
}
