package dev.uhet.s99.lists

import scala.annotation.tailrec

object p12 {

  def repeat[A](len: Int, e: A): List[A] = {

    @tailrec
    def inner(len: Int, acc: List[A]): List[A] = {
      if (len == 0) acc
      else inner(len - 1, e :: acc)
    }

    inner(len, List.empty)
  }

  def decode[A](list: List[(Int, A)]): List[A] = {

    @tailrec
    def inner(list: List[(Int, A)], acc: List[A]): List[A] = {
      list match {
        case Nil => acc
        case (len, e) :: xs => inner(xs, acc ++ repeat(len, e))
      }

    }

    inner(list, List.empty)
  }

}
