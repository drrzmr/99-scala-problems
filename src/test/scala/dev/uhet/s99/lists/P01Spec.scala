package dev.uhet.s99.lists

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class P01Spec extends AnyFlatSpec with Matchers {

  "P01 last" should  "Find the last element of a list" in {
    p01.last(List(1, 2, 3, 4)) should be (Some(4))
    p01.last(List(1)) should be (Some(1))
    p01.last(List.empty[Int]) should be (None)
  }
}
