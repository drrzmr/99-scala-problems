package dev.uhet.s99.lists

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class P02Spec extends AnyFlatSpec with Matchers {

  "P02 penultimate" should "Find the last but one element of a list" in {
    p02.penultimate(List(1, 2, 3, 4)) should be (Some(3))
    p02.penultimate(List(1, 2, 3)) should be (Some(2))
    p02.penultimate(List(1, 2)) should be (Some(1))
    p02.penultimate(List(1)) should be (None)
    p02.penultimate(List.empty[Int]) should be (None)
  }
}
