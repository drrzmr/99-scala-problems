package dev.uhet.s99.lists

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class P03Spec extends AnyFlatSpec with Matchers {

  "P03 nth" should "Find the Kth element of a list" in {
    p03.nth(0, List(1, 2, 3, 4)) should be (Some(1))
    p03.nth(3, List(1, 2, 3, 4)) should be (Some(4))
    p03.nth(-1, List(1, 2, 3, 4)) should be (None)
    p03.nth(4, List(1, 2, 3, 4)) should be (None)
    p03.nth(0, List.empty[Int]) should be (None)
    p03.nth(1, List.empty[Int]) should be (None)
    p03.nth(-1, List.empty[Int]) should be (None)
  }
}
