package dev.uhet.s99.lists

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class P04Spec extends AnyFlatSpec with Matchers {

  "P04 length" should "Find the number of elements of a list" in {
    p04.length(List(1, 2, 3, 4)) should be (4)
    p04.length(List(1)) should be (1)
    p04.length(List.empty[Int]) should be (0)
  }
}
