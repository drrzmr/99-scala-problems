package dev.uhet.s99.lists

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class P05Spec extends AnyFlatSpec with Matchers {

  "P05 reverse" should "Reverse a list" in {
    p05.reverse(List(1, 2, 3, 4)) should be (List(4, 3, 2, 1))
    p05.reverse(List.empty) should be (List.empty)
    p05.reverse(Nil) should be (Nil)
  }
}
