package dev.uhet.s99.lists

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class P08Spec extends AnyFlatSpec with Matchers {

  "P08 compress" should "Eliminate consecutive duplicates of list elements" in {
    p08.compress(List(1, 1, 1, 2, 3)) should be(List(1, 2, 3))
    p08.compress(List(1, 1, 1, 2, 2, 3, 1)) should be(List(1, 2, 3, 1))

    p08.compress(List.empty) should be(List.empty)
    p08.compress(Nil) should be(Nil)
  }
}
