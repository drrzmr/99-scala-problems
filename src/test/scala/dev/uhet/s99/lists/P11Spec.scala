package dev.uhet.s99.lists

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class P11Spec extends AnyFlatSpec with Matchers {

  "P11 length modified enconding of list" should "works" in {
    p11.encodeModified(List(1, 1, 3, 2, 2, 1)) should be (List((2, 1), 3, (2, 2), 1))
  }
}
