package dev.uhet.s99.lists

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class P12Spec extends AnyFlatSpec with Matchers {

  "P12 repeat" should "works" in {
    p12.repeat(1, 'a') should be (List('a'))
    p12.repeat(2, 'a') should be (List('a', 'a'))
  }

  "P12 decode" should "works" in {
    p12.decode(List((1, 'a'), (3, 'b'), (1, 'z'), (2, 'y'))) should be (List('a', 'b', 'b', 'b', 'z', 'y', 'y'))
  }
}
